package ClasesRe;


import ClasesRe.ReciboNom;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Black
 */
public class ReciboNom02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ReciboNom ReciboNom02 = new ReciboNom();
        ReciboNom02.setNumRe(23);
        ReciboNom02.setNom("josé lopez Acosta");
        ReciboNom02.setNivel(1);
        ReciboNom02.setPuesto(2);
        ReciboNom02.setDias(10);
         
        System.out.println("Num. Recibo: "+ ReciboNom02.getNumRe());
        System.out.println("Nombre: "+ ReciboNom02.getNom());
        System.out.println("Puesto: "+ ReciboNom02.getPuesto()+(" albañil"));
        System.out.println("Nivel: " + ReciboNom02.getNivel()+(" base"));
        System.out.println("días trabajados: "+ ReciboNom02.getDias()+(" Días trabajados"));
        System.out.println("Calculo de pago subtotal:"+ ReciboNom02.calcularPago());
        System.out.println("calculo de pago con impuesto:"+ ReciboNom02.calcularImpuesto());
        System.out.println("calculo de pago total:"+ ReciboNom02.calcularTotal());
        
    }
    
}
