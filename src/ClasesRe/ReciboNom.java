package ClasesRe;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Black
 */
public class ReciboNom {
       
    private int numRe;
    private String nom;
    private int nivel;
    private int puesto;
    private int dias;
  
    
    public ReciboNom(){
    this.numRe=0;
    this.nom="";
    this.nivel=0;
    this.puesto=0;
    this.dias=0;
    
    
    }
    //Constructor por argumentos
    public ReciboNom(int numRe, String nom, int nivel, int puesto, int dias){
    this.numRe=numRe;
    this.nom=nom;
    this.nivel=nivel;
    this.puesto=puesto;
    this.dias=dias;
   
    }
    
    //Copia "OTRO"
    public ReciboNom(ReciboNom otro){
    this.numRe=otro.numRe;
    this.nom=otro.nom;
    this.nivel=otro.nivel;
    this.puesto=otro.puesto;
    this.dias=otro.dias;

    }
    

    //Metodos Set y Get

    public int getNumRe() {
        return numRe;
    }

    public void setNumRe(int numRe) {
        this.numRe = numRe;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
    
       //Metodos de comportamiento

     public float calcularPago(){
    float pago=0.0f;
    if (this.puesto == 1) {
        pago=(this.dias*100);
    }
    if (this.puesto == 2) {
        pago=(this.dias*200);
    }
    if (this.puesto == 3) {
         pago=(this.dias*300);
    }
    return pago;
    }
    
    public float calcularImpuesto(){
    float impuesto=0.0f;
    if (this.nivel == 1) {
        impuesto=(this.calcularPago()*0.05f);
    }
    if (this.nivel == 2) {
        impuesto=(this.calcularPago()*0.03f);
    }
    return impuesto;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularPago()-this.calcularImpuesto();
    return total;
    }
    
}



